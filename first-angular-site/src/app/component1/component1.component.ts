import { Component, Input, EventEmitter, Output } from '@angular/core';
import { props } from '@ngrx/store';
import { Course } from '../models/course';

@Component({
  selector: 'app-component1',
  templateUrl: './component1.component.html',
  styleUrls: ['./component1.component.scss']
})
export class Component1Component {
  // @Input() text: string;
  @Input()
  course:Course;
  @Input()
  cardNumber: number;
  @Output()
  courseSelect = new EventEmitter<Course>();
  

  viewCourse(){
    console.log('view course button clicked');
    this.courseSelect.emit(this.course);
  }
}
