import { Component, EventEmitter } from '@angular/core';
import { COURSES } from './store';
import { Course } from './models/course';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  courses = COURSES;

  data = {
    ob1: COURSES[0],
    title: 'first-angular-site',
    inputText: 'Text'
  };
  clickEvent(){
    alert(this.data.title);
  }
  keyUp(value: string){
    this.data.inputText = value;
    console.log(`inputText value was updated to ${value}`);
  }
  onCourseSelected(course: Course){
    console.log('selected...', course)
  }
}
