export interface Course {
    id: number,
    text: string,
    description: string
}