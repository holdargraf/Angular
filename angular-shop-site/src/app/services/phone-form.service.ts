import { Injectable } from "@angular/core";
import { DataService } from "./data.service";
import { Phone } from "../models/phone.model";

@Injectable()
export class PhoneFormService {
    constructor(private dataService: DataService){}

    formType : string;
    setFormType(newFormType: string): void{
        this.formType = newFormType;
    }

    validatePhoneValues(phone: Phone): boolean{
        let errorValues: any = [null, undefined, ""]
        if(errorValues.includes(
            phone.id,
            phone.imgSrc, 
            phone.name, 
            phone.brand, 
            phone.categoryId, 
            phone.colors, 
            phone.country, 
            phone.description
        )){
            return false;
        }
        return true;
    }

    savePhone(phone: Phone) : void{
        if(this.formType == "Edit"){
            //console.log(this.formType)
            this.dataService.updatePhone(phone)
        }
        else if(this.formType == "Add"){
            //console.log(this.formType)
            this.dataService.addPhone(phone)
        }
        else{
            throw new Error("Wrond formType value");
        }
    }

    sendToDatabase() : void{
        console.log("send to database is working . .")
    }
}