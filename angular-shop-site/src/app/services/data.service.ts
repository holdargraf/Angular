import { Category } from "../models/category.model";
import { Phone } from "../models/phone.model";
import categoryJSON from "../../assets/json/category.json"
import phonesJSON from "../../assets/json/phones.json"
import { Injectable } from "@angular/core";

@Injectable()
export class DataService {
    constructor(){}

    phonesData : Phone[] = phonesJSON as Phone[];
    categoriesData : Category[] = categoryJSON as Category[];

    findPhonebyId(id : number): Phone {
        for(let i = 0; i < phonesJSON.length; i++){
            if(phonesJSON[i].id == id){
                return phonesJSON[i];
            }
        }
        return {
            id: 0,
            categoryId: 0,
            name: '',
            description: '',
            country: '',
            colors: [''],
            brand: '',
            imgSrc: '',
        } as Phone;
    }

    addPhone(value: Phone) : void{
        console.log(value);
        this.phonesData.push(value);
    }

    addCategory(value: Category) : void{
        this.categoriesData.push(value);
    }

    updateCategory(value: Category) : void{
        this.categoriesData.forEach(category => {
            if(category.id == value.id){
                category.title = value.title
            }
        });
    }

    updatePhone(value: Phone) : void{
        this.phonesData.forEach(phone => {
            if(phone.id == value.id){
                phone.name = value.name
                phone.brand = value.brand
                phone.colors = value.colors
                phone.country = value.country
                phone.description = value.description
            }
        });
    }

    deleteCategory(value: Category) : void{
        for(let i = 0; i < this.categoriesData.length; i++){
            if(this.categoriesData[i].id == value.id){
                this.categoriesData.splice(i,1);
                break;
            }
        }
    }

    deletePhone(value: Phone) : void{
        for(let i = 0; i < this.phonesData.length; i++){
            if(this.phonesData[i].id == value.id){
                this.phonesData.splice(i,1);
                break;
            }
        }
    }
}