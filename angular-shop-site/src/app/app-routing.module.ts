import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MainComponent } from './components/main/main.component';
import { CategoryListComponent } from './components/categories/list/category-list.component';
import { PhoneListComponent } from './components/phones/list/phone-list.component';
import { PhoneDetailComponent } from './components/phones/detail/phone-detail.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

const routes: Routes = [
  {
    path: '',
    component: 
    MainComponent
  },
  {
    path: 'category', 
    component: CategoryListComponent
  },
  {
    path: 'phone/:id/:name',
    component: PhoneListComponent
  },
  {
    path: 'form/:formType/:id', 
    component: PhoneDetailComponent, 
    outlet: 'form'
  },
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
