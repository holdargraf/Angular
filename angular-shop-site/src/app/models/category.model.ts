export interface Category{
    id: number;
    title: string;
    imgSrc: string;
}