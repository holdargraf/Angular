export interface Phone{
    id: number,
    categoryId: number,
    name: string,
    description: string,
    country: string,
    colors: string[],
    brand: string,
    imgSrc: string,
}