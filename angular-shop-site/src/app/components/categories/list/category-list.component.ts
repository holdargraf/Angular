import { Component, Input } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { Category } from 'src/app/models/category.model';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss'],
  providers: [DataService]
})
export class CategoryListComponent {

  constructor(private dataService: DataService){}
  categories : Category[] = this.dataService.categoriesData;
  
  formStatus : boolean = false;

  showCategoryForm() : void{
    this.formStatus = !this.formStatus;
  }

}
