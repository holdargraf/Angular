import { Component, Input } from '@angular/core';
import { Category } from '../../../models/category.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-category-list-item',
  templateUrl: './category-list-item.component.html',
  styleUrls: ['./category-list-item.component.scss'],
  providers:[DataService]
})
export class CategoryListItemComponent {
  constructor(private dataService: DataService){}

  @Input()
  category: Category;

  formStatus : boolean = false;

  deleteCategory() : void{
    this.dataService.deleteCategory(this.category)
  }

  showEditForm() : void{
    this.formStatus = !this.formStatus
  }
  // @Input()
  // fontColor: string;
  getStyle(): {[style: string]: string} {
    return {'background-image': `url(${this.category.imgSrc})`};
  }
  // getClass(){
  //   if(this.fontColor=='white'){
  //     return {'white-text': true};
  //   }
  //   else{
  //     return {'white-text': false};
  //   }
  // }  
  getCategoryId(): number{
    return this.category.id;
  }
  getCategoryName(): string{
    return this.category.title;
  }
}

