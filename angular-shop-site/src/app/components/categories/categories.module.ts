import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule }   from '@angular/forms';

import { CategoryListItemComponent } from './list/category-list-item.component';
import { CategoryListComponent } from './list/category-list.component';
import { AppRoutingModule } from '../../app-routing.module';


@NgModule({
  declarations: [
    CategoryListItemComponent,
    CategoryListComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    ReactiveFormsModule
  ],
})
export class CategoriesModule { }
