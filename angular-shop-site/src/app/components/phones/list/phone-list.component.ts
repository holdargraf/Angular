import { Component, SimpleChanges  } from '@angular/core';
import { DataService } from '../../../services/data.service';
import { Phone } from 'src/app/models/phone.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-phone-list',
  templateUrl: './phone-list.component.html',
  styleUrls: ['./phone-list.component.scss'],
  providers: [DataService]
})
export class PhoneListComponent {

  private routeSubscription: Subscription;
  constructor(
    private activatedRoute: ActivatedRoute,
    private dataService: DataService
  )
  {  
    this.routeSubscription = activatedRoute.params.subscribe(
      (params: any) => {
        this.categoryId=params['id']
        this.categoryName=params['name']
      }
    );
  }
  ngOnInit(): void {
    let formRouterValues = ['form', 'Add', this.categoryId];
  }
  formRouterValues : any
  phones : Phone[] = this.dataService.phonesData;
  phone : Phone;
  categoryId : number;
  categoryName : string;
  formStatus : boolean = false;
  formType : string = "Add";

  showForm() : void{
    this.formStatus = !this.formStatus
    console.log(this.formStatus)
  }
  closeForm() : void{
    this.formStatus = false
  }

  switchRoute() : void{
    if(this.formRouterValues == null){
      this.formRouterValues = ['form', 'Add', this.categoryId];
    }
    else{
      this.formRouterValues = null
    }
  }

  ngDoCheck(): void {
    if(history.state.closeButton){
      this.closeForm()
      this.switchRoute()
      history.state.closeButton = false
    }
  }
  
}
