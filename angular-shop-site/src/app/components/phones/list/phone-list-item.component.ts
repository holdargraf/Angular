import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Phone } from '../../../models/phone.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-phone-list-item',
  templateUrl: './phone-list-item.component.html',
  styleUrls: ['./phone-list-item.component.scss'],
  providers:[DataService]
})
export class PhoneListItemComponent {

  constructor(private dataService: DataService){}
  @Input()
  phone: Phone;
  
  @Output()
  switchAddButtonStatusEmitter = new EventEmitter()

  formStatus : boolean = false;
  
  getBackgroundImg(): {[style: string]: string}{
    return {'background-image': `url(${this.phone.imgSrc})`};
  }
  
  deleteCategory() : void{
    this.dataService.deletePhone(this.phone)
  }

  switchAddButtonStatus() : void{
    this.switchAddButtonStatusEmitter.emit()
  }

  deletePhone() : void{
    this.dataService.deletePhone(this.phone)
  }
}
