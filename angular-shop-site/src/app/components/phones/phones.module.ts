import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule }   from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { PhoneListComponent } from './list/phone-list.component';
import { PhoneListItemComponent } from './list/phone-list-item.component';
import { AppRoutingModule } from '../../app-routing.module';
import { PhoneDetailComponent } from './detail/phone-detail.component';


@NgModule({
  declarations: [
    PhoneListComponent,
    PhoneListItemComponent,
    PhoneDetailComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgMultiSelectDropDownModule.forRoot()
  ]
})
export class PhonesModule { }
