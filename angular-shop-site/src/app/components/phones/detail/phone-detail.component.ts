import { Component, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl} from '@angular/forms';
import { DataService } from 'src/app/services/data.service';
import { PhoneFormService } from 'src/app/services/phone-form.service';
import { Phone } from 'src/app/models/phone.model';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router'
import { IDropdownSettings } from 'ng-multiselect-dropdown';

@Component({
  selector: 'app-phone-detail',
  templateUrl: './phone-detail.component.html',
  styleUrls: ['./phone-detail.component.scss'],
  providers: [DataService, PhoneFormService]
})
export class PhoneDetailComponent {
  private routeSubscription: Subscription;
  constructor(
    private route: ActivatedRoute,
    private dataService: DataService,
    private phoneFormServise: PhoneFormService
  )
  {  
    this.routeSubscription = route.params.subscribe(
      (params: any) => {
        this.formType=params['formType']
        if(this.formType=="Add"){
          this.categoryId = params['id']
        }
        else if(this.formType=="Edit"){
          this.phoneId = params['id']
        }
      }
    )
  }
  phones: Phone[] = this.dataService.phonesData;
  phoneForm: FormGroup;
  formType: string;
  categoryId: number;
  phoneId: number;
  phone: Phone;
  maxOfPhonesId: number = Math.max(...this.phones.map((el) => el.id));
  dropdownList: { item_id: number, item_text: string }[] = [
    { item_id: 1, item_text: 'White' },
    { item_id: 2, item_text: 'Black' },
    { item_id: 3, item_text: 'Gray' },
    { item_id: 4, item_text: 'Gold' },
    { item_id: 5, item_text: 'Silver' },
    { item_id: 6, item_text: 'Red' },
    { item_id: 7, item_text: 'Green' },
    { item_id: 8, item_text: 'Blue' }
  ];
  selectedItems: { item_id: number, item_text: string }[];
  dropdownSettings: IDropdownSettings;
  dropDownForm: FormGroup;
  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }
  ngOnInit(): void {
    
    this.phone = this.dataService.findPhonebyId(this.phoneId);
    if(this.formType == "Add"){
      this.clearPhoneValue()
    }
    
    this.selectedItems = this.findSelectedColors(this.phone.colors);
    
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      allowSearchFilter: true,
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All'
    };
    
    this.phoneForm = new FormGroup({
      id: new FormControl(this.getUniqueIdForPhone()),
      categoryId: new FormControl(this.categoryId),
      name: new FormControl(this.phone.name),
      description: new FormControl(this.phone.description),
      country: new FormControl(this.phone.country),
      brand: new FormControl(this.phone.brand),
      imgSrc: new FormControl('assets/img/ProductsImg/GraniteGray.jpg'),
      colors: new FormControl(this.selectedItems)
    });
  }
  clearPhoneValue(): void {
    this.phone = {
      id: 0,
      categoryId: 0,
      name: '',
      description: '',
      country: '',
      colors: [],
      brand: '',
      imgSrc: '',
    } as Phone;
  }

  getUniqueIdForPhone(): number {
    if (this.formType == 'Add') {
      this.maxOfPhonesId++;
      return this.maxOfPhonesId;
    } else {
      return this.phone.id;
    }
  }

  findSelectedColors(colors: string[]) : { item_id: number, item_text: string }[] {
    let result : { item_id: number, item_text: string }[] = [];
    colors.forEach(color => {
      this.dropdownList.forEach(listColor => {
        if(color == listColor.item_text){
          result.push({item_id: listColor.item_id, item_text: listColor.item_text})
        }
      });
    });
    return result;
  }
  convertSelectedColorsToArr(colors: [{[id: string]: number | string}]): string[]{
    let result: string[] = []
    for (let key in this.selectedItems) {
      result.push(this.selectedItems[key].item_text)
    }
    return result;
  }

  savePhone(): void {
    console.log('submit is work');

    let phone: Phone = {
      id: this.phoneForm.value.id,
      categoryId: this.categoryId,
      name: this.phoneForm.value.name,
      description: this.phoneForm.value.description,
      country: this.phoneForm.value.country,
      colors: this.convertSelectedColorsToArr(this.phoneForm.value.colors),
      brand: this.phoneForm.value.brand,
      imgSrc: this.phoneForm.value.imgSrc,
    } as Phone;
    
    this.phoneFormServise.setFormType(this.formType)

    if(this.phoneFormServise.validatePhoneValues(phone)){
      this.phoneFormServise.savePhone(phone)
      this.phoneFormServise.sendToDatabase()
    }
    else{
      throw new Error('Validation fail: Wrong phone values')
    }
  }
}
